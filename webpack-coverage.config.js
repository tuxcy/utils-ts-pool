const path = require('path');

module.exports = (env, argv) => {
    return {
        node: {
            fs: 'empty'     // skip warning on lokijs 'fs' dep missing
        },
        devServer: {
            contentBase: path.join(__dirname, 'test/test-app'),
            compress: true,
            port: 9100
        },
        entry: './test/index.spec.ts',
        devtool: 'inline-source-map',
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    include: path.resolve(`src/`), // instrument only sources with Istanbul
                    exclude: [/node_modules/],
                    loader: 'istanbul-instrumenter-loader',

                },
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    options: {
                        configFile: 'tsconfig.json',
                        compilerOptions: {}
                    }
                }
            ]
        },
        mode: 'development',
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            filename: 'test.js',
            path: path.resolve(__dirname, 'test/test-app')
        },
        plugins: []
    };
};
