import {WorkerPool} from "../interface/WorkerPool";
import {Task} from "../interface/Task";
import {Worker} from "../interface/Worker";
import {TaskProcessBehaviour, TaskState, WorkerState} from "../enum/WorkerEnum";

class WorkerPoolImpl implements WorkerPool {
    private static MAX_CACHE_TASKS_LENGTH: number = 20;
    private workers: Worker[] = [];
    private tasks: Task[] = [];
    private finishCount: number = 0;
    private promise: Promise<void> = null;
    private promiseCallback: any = {
        resolve: null,
        reject: null
    };

    addTask(tasks: Task[]): Promise<void> {
        this.tasks = this.tasks.concat(tasks);
        this.affectTaskToWorker();

        if (!this.promise) {
            this.promise = new Promise<void>((resolve, reject) => {
                this.promiseCallback.resolve = resolve;
                this.promiseCallback.reject = reject;
            });
        }
        return this.promise;
    }
    addWorkers(workers: Worker[]): void {
        this.workers = this.workers.concat(workers.map((el: Worker) => {
            el.linkPool(this);
            return el;
        }));
        this.affectTaskToWorker();
    }
    removeWorkers(workers: Worker[]): Worker[] {
        let res: Worker[] = [];
        let ids: string[] = workers.map((el: Worker) => el.getId());

        this.workers = this.workers.filter((el: Worker) => {
            let state: boolean = [WorkerState.ERROR, WorkerState.IDLE].includes(el.getState()) &&
                ids.includes(el.getId());

            if (!state) {
                res.push(el);
                el.unlinkPool();
            }

            return state;
        });

        return res;
    }
    getTasks(): Task[] {
        return this.tasks;
    }
    getWorkers(): Worker[] {
        return this.workers;
    }
    triggerWorkerAvailability(worker: Worker): void {
        if (worker.getState() === WorkerState.RUNNING) {
            console.warn(`worker '${worker.getId()}' is still running`);
            return
        } else if (worker.getState() === WorkerState.ERROR) {
            console.warn(`worker '${worker.getId()}' has ended with an error`);
        }
        this.finishCount += 1;
        this.affectTaskToWorker();
    }

    /** utils **/

    private getUnaffectTask(): Task[] {
        let res: Task[] = [];

        this.tasks.forEach((task: Task) => {
            if (task.getState() === TaskState.PENDING) {
                res.push(task);
            }
        });

        return res;
    }
    private affectTaskToWorker(): void {
        this.gcTasks(); // clean tasks cache array

        if (this.tasks.length < WorkerPoolImpl.MAX_CACHE_TASKS_LENGTH &&
            this.getAvailableWorkers().length === this.workers.length &&
            this.promise) {
            if (this.tasks.every((el) => {
                return [TaskState.ERROR, TaskState.SUCCESS].includes(el.getState())
            })) {
                this.promiseCallback.resolve();
                this.promise = null;
            }
        }

        while (this.getUnaffectTask().length && this.getAvailableWorkers().length) {
            let task: Task = this.getUnaffectTask().pop();
            let worker: Worker = this.getAvailableWorkers().pop();

            worker.linkTask(task);

            if (task.getBehaviour() === TaskProcessBehaviour.IMMEDIATE) {
                worker.work();
            }
        }
    }
    private getAvailableWorkers(): Worker[] {
        return this.workers.filter((el: Worker) => {
            return [WorkerState.IDLE].includes(el.getState());
        })
    }
    private gcTasks(force: boolean = false): void {
        if (!force && this.finishCount < WorkerPoolImpl.MAX_CACHE_TASKS_LENGTH) {
            return;
        }

        let diff: number = 0;
        let i = 0;
        while (this.tasks.length > i) {
            if ([TaskState.SUCCESS, TaskState.ERROR].includes(this.tasks[i].getState())) {
                this.tasks.splice(i, 1);
                diff = diff + 1;
            } else {
                i += 1;
            }
        }
        this.finishCount = 0;
        console.debug(`removing ${diff} tasks new task queue has size ${this.tasks.length}`);
    }
}

export {
    WorkerPoolImpl
}
