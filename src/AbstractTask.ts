import {Task} from "./interface/Task";
import * as uuidv4 from 'uuid/v4';
import {TaskProcessBehaviour, TaskState} from "./enum/WorkerEnum";

abstract class AbstractTask implements Task {
    private readonly id: string;
    private readonly processBehaviour: TaskProcessBehaviour;
    protected state: TaskState;
    protected errorCb: any = () => console.log("no error callback defined");
    protected successCb: any = () => console.log("no success callback defined");

    protected constructor(behave = TaskProcessBehaviour.IMMEDIATE) {
        this.id = uuidv4();
        this.processBehaviour = behave;
        this.state = TaskState.PENDING;
    }

    getId(): string {
        return this.id;
    }
    getState(): TaskState {
        return this.state;
    }
    getBehaviour(): TaskProcessBehaviour {
        return  this.processBehaviour;
    }
    setErrorCallback(cb: any) {
        this.errorCb = cb;
    }
    setSuccessCallback(cb: any) {
        this.successCb = cb;
    }

    abstract process(args: any): Promise<void>;
}

export {
    AbstractTask
}
