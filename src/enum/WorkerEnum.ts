enum WorkerState {
    IDLE = 'IDLE',
    RUNNING = 'RUNNING',
    ERROR = 'ERROR'
}

enum TaskState {
    PENDING = 'PENDING',
    RUNNING = 'RUNNING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR'
}

enum TaskProcessBehaviour {
    IMMEDIATE = 'IMMEDIATE'
}

export {
    WorkerState,
    TaskState,
    TaskProcessBehaviour
}
