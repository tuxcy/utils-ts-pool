import {Worker} from "./interface/Worker";
import {Task} from "./interface/Task";
import {WorkerPool} from "./interface/WorkerPool";
import {WorkerState} from "./enum/WorkerEnum";
import * as uuidv4 from 'uuid/v4';

abstract class AbstractWorker implements Worker {
    private readonly id: string;
    protected task: Task;
    protected pool: WorkerPool;
    protected state: WorkerState;

    protected constructor() {
        this.id = uuidv4();
        this.state = WorkerState.IDLE;
    }

    getId(): string {
        return this.id;
    }
    getState(): WorkerState {
        return this.state;
    }
    linkTask(task: Task): Worker {
        this.task = task;
        return this;
    }
    linkPool(pool: WorkerPool): Worker {
        this.pool = pool;
        return this;
    }
    unlinkTask(): void {
        this.task = null;
    }
    unlinkPool(): void {
        this.pool = null;
    }
    abstract work(): Promise<void>

    protected successWorkerCallback() {
        let self = this;
        return function () {
            console.log(`worker ${self.getId()} is calling success callback`);
            self.state = WorkerState.IDLE;
            self.pool.triggerWorkerAvailability(self);
        }
    }

    protected errorWorkerCallback() {
        let self = this;
        return function () {
            console.log(`worker ${self.getId()} is calling error callback`);
            self.state = WorkerState.ERROR;
            self.pool.triggerWorkerAvailability(self);
        }
    }
}

export {
    AbstractWorker
}
