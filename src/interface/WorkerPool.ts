import {Worker} from "./Worker";
import {Task} from "./Task";

interface WorkerPool {
    addWorkers(workers: Worker[]): void
    addTask(tasks: Task[]): Promise<void>
    removeWorkers(workers: Worker[]): Worker[]
    getWorkers(): Worker[]
    getTasks(): Task[]
    triggerWorkerAvailability(worker: Worker): void
}

export {
    WorkerPool
}
