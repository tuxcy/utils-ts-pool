import {TaskProcessBehaviour, TaskState} from "..";

interface Task {
    getId(): string
    getState(): TaskState;
    getBehaviour(): TaskProcessBehaviour;
    process(args: any): Promise<void>
    setSuccessCallback(cb: any)
    setErrorCallback(cb: any)
}

export {
    Task
}
