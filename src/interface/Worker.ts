import {WorkerState} from "../enum/WorkerEnum";
import {WorkerPool} from "./WorkerPool";
import {Task} from "./Task";

interface Worker {
    getId(): string
    getState(): WorkerState
    work(): Promise<void>

    linkTask(task: Task): Worker
    unlinkTask(): void
    linkPool(pool: WorkerPool): Worker
    unlinkPool(): void
}

export {
    Worker
}
