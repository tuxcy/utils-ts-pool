import {assert, expect} from "chai";
import {WorkerPool, WorkerPoolImpl} from "../src";
import {MockWorker} from "./mock/MockWorker";
import {MockTask} from "./mock/MockTask";

describe('WorkerPool', function () {
    let pool: WorkerPool;

    beforeEach(function () {
        pool = new WorkerPoolImpl();
    });

    it('add and remove Worker and Task and run a simple process', async function () {
        let worker1 = new MockWorker();
        let worker2 = new MockWorker();

        assert.equal(pool.getTasks().length, 0);
        assert.equal(pool.getWorkers().length, 0);

        pool.addWorkers([worker1, worker2]);
        assert.equal(pool.getWorkers().length, 2);
        pool.removeWorkers([worker2]);
        assert.equal(pool.getWorkers().length, 1);

        let tasks = [
            new MockTask(2),
            new MockTask(20),
            new MockTask(225),
            new MockTask(1),
            new MockTask(0)
        ];

        let start = Date.now();
        let lock = pool.addTask(tasks);

        await lock;
        let end = Date.now();

        (pool as any).gcTasks(true);
        assert.equal(pool.getTasks().length, 0);

        expect(end - start).to.be.lessThan(100);
        return
    });

    it('Processing many task with few worker', async function () {
        this.timeout(5 * 1000);
        let workers = [
            new MockWorker(),
            new MockWorker()
        ];
        pool.addWorkers(workers);
        assert.equal(pool.getTasks().length, 0);
        assert.equal(pool.getWorkers().length, workers.length);

        let lock = null;
        let start = Date.now();
        Array(100).fill(1).forEach(function (el) {
            lock = pool.addTask([
                new MockTask(2),
                new MockTask(20),
                new MockTask(225),
                new MockTask(1),
                new MockTask(20)
            ]);
        });

        await lock;
        let end = Date.now();


        (pool as any).gcTasks(true);
        assert.equal(pool.getTasks().length, 0);
        expect(end - start).to.be.greaterThan(1 * 1000);
    });


    it('Processing many task with many worker', async function () {
        this.timeout(5 * 1000);
        let workers = [
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker(),
            new MockWorker()
        ];
        pool.addWorkers(workers);
        assert.equal(pool.getTasks().length, 0);
        assert.equal(pool.getWorkers().length, workers.length);

        let lock = null;
        let start = Date.now();
        Array(100).fill(1).forEach(function (el) {
            lock = pool.addTask([
                new MockTask(2),
                new MockTask(20),
                new MockTask(225),
                new MockTask(1),
                new MockTask(20)
            ]);
        });

        await lock;
        let end = Date.now();
        (pool as any).gcTasks(true);

        assert.equal(pool.getTasks().length, 0);
        expect(end - start).to.be.lessThan(1 * 1000);
    });
});
