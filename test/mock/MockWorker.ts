import {AbstractWorker} from "../../src/AbstractWorker";
import {WorkerState} from "../../src";

class MockWorker extends AbstractWorker {
    constructor() {
        super();
    }

    async work() {
        if (!this.task) {
            console.error(`worker ${this.getId()} to process without task`);
            this.state = WorkerState.ERROR;
            this.errorWorkerCallback()
        } else {
            this.state = WorkerState.RUNNING;
            this.task.setErrorCallback(this.errorWorkerCallback());
            this.task.setSuccessCallback(this.successWorkerCallback());
            console.log(`processing task ${this.task.getId()}`);
            this.task.process(null);
        }
    }
}

export {
    MockWorker
}
