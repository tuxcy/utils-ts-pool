import {AbstractTask} from "../../src/AbstractTask";
import {TaskState} from "../../src";

class MockTask extends AbstractTask {
    private repetition: number = 0;
    private result: number[] = [];

    public constructor(repetition: number) {
        super();
        this.repetition = repetition
    }

    async process(args: any = null) {
        this.state = TaskState.RUNNING;
        this.result = Array(this.repetition).fill(1);
        await this.sleep();
        this.state = TaskState.SUCCESS;
        this.successCb();
    }

    private async sleep(): Promise<void> {
        let p = new Promise<void>((resolve, reject) => {
            setTimeout(function () {
                resolve();
            }, 10)
        });
        return p;
    }
}

export {
    MockTask
}
