const path = require('path');
const TsDeclarationWebpackPlugin = require('ts-declaration-webpack-plugin');

var devPlugins = [

];
var prodPlugins = [
    new TsDeclarationWebpackPlugin({
        name: 'index.d.ts'
    })
];
module.exports = (env, argv) => {
    return {
        node: {
            fs: 'empty'     // skip warning on lokijs 'fs' dep missing
        },
        entry: './src/index.ts',
        // devtool: 'inline-source-map',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    options: {
                        configFile: 'tsconfig.json',
                        compilerOptions: {}
                    }
                }
            ]
        },
        mode: process.env.MODE,
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            filename: process.env.MODE === 'production' ? 'index.js' : 'index-debug.js',
            path: path.resolve(__dirname, 'dist'),
            libraryTarget: 'commonjs2',
        },
        plugins: process.env.MODE === 'production' ? prodPlugins : devPlugins
    };
};
